<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class RegistrationTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->visit('/registration/create')
            ->type('Test User', 'name')
            ->type('male', 'gender')
            ->type('test@email.com', 'email')
            ->type('Testing2015!', 'password')
            ->type('Testing2015!', 'password_confirmation')
            ->press('Submit')
            ->see('User successfully created.');
    }
}
