<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\UserFormRequest;

class RegistrationController extends Controller
{
    /**
     * Show the form for creating a new user.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('registration.create');
    }

    /**
     * Store a newly created user.
     * Injecting UserFormRequest to validate form post data
     * @param  UserFormRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserFormRequest $request)
    {
        $user = new User([
            'name'      => $request->get('name'),
            'gender'    => $request->get('gender'),
            'email'     => $request->get('email'),
            'password'  => Hash::make($request->get('password'))
        ]);

        if($user->save()){
            return[
                'success' => true,
                'text' =>'User successfully created.'
            ];
        }else{
            return[
                'success' => false,
                'text' =>'Can not register user.'
            ];
        }
    }

}
