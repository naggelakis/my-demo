@extends('layouts.main')

@section('content')
    <div class="row">
        <h1>Registration Form</h1>
        <ul>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li> @endforeach
        </ul>

        @if(Session::has('message'))
            <div class="alert alert-info">
                {{ Session::get('message') }}
            </div>
        @endif

        <div class="message"></div>
        <div class="col-md-4 col-md-offset-4">
            <form id="registrationForm" method="post" action="/registration">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" class="form-control" id="name" name="name" required>
                </div>
                <div class="form-group">
                    <label for="gender">Gender</label>
                    <select class="form-control" name="gender" id="gender">
                        <option value="default">Choose...</option>
                        <option value="male">male</option>
                        <option value="female">female</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="email">Email address</label>
                    <input type="email" class="form-control" id="email" name="email" required>
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" class="form-control" id="password" name="password" required>
                </div>
                <div class="form-group">
                    <label for="password_confirmation">Confirm Password</label>
                    <input type="password" class="form-control" name="password_confirmation" id="password_confirmation">
                </div>
                <button type="submit" class="btn btn-default" id="formSubmit">Submit</button>
            </form>
        </div>
    </div>
@stop
@section('scripts')
    <script>
        $(document).ready(function(){

            var form = $("#registrationForm");

            $.validator.addMethod("valueNotEquals", function(value, element, arg){
                return arg != value;
            }, "Value must not equal arg.");

            $.validator.addMethod("pwdcheck", function(value) {
                return /^.*(?=.{8,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%&!]).*$/.test(value); // has a digit
            });

            $(form).validate({
                rules: {
                    name: "required",

                    gender: {
                        valueNotEquals: "default"
                    },

                    email: {
                        required: true,
                        email: true
                    },

                    password : {
                        required: true,
                        minlength: 8,
                        pwdcheck: true

                    },
                    password_confirmation : {
                        required: true,
                        equalTo : "#password"
                    }

                },
                messages: {
                    name: "<span class='text-danger'>Required field</span>",

                    gender: {
                        valueNotEquals: "<span class='text-danger'>Please select an item!</span>"
                    },

                    email: {
                        required: "<span class='text-danger'>We need your email address to contact you</span>",
                        email: "<span class='text-danger'>Your email address must be in the format of name@domain.com</span>"
                    },
                    password: {
                        required: "<span class='text-danger'>Required field</span>",
                        minlength: "<span class='text-danger'>Password should be at least 8 characters long</span>",
                        pwdcheck: "<span class='text-danger'>Password must contain at least one number, one uppercase letter and one special character</span>"
                    },
                    password_confirmation: {
                        required: "<span class='text-danger'>Required field</span>",
                        equalTo: "<span class='text-danger'>Passwords do not match</span>"
                    }
                },

                highlight: function(element) {
                    jQuery(element).closest('.form-group').removeClass('has-success').addClass('has-error');
                },
                success: function(element) {
                    jQuery(element).closest('.form-group').removeClass('has-error');
                }
            });

            $(form).on('submit', function(){
                $.ajax({
                    type:'POST',
                    url:'/registration',
                    data:$(form).serialize(),
                    dataType: 'json'
                }).done(function(data){
                    if(data.success){
                        $(".message").html("<p class='alert-success'>"+data.text+"</p>");
                    }else{
                        $(".message").html("<p class='alert-danger'>"+data.text+"</p>");
                    }
                });
                return false;
            });

        });
    </script>
@stop
